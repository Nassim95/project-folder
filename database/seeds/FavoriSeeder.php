<?php

use Illuminate\Database\Seeder;

class FavoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Favori::class, 5)->create();
    }
}
