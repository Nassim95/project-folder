<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersCurrentEpisodeShows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_current_episode_shows', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user');
            $table->integer('id_show');
            $table->integer('season');
            $table->integer('episode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_current_episode_shows');
    }
}
