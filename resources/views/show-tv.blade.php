@extends('layouts.app')

@section('content')

<div class="movie-info">
    <div class="col-md-6" style="margin:auto;">
        <div class="card flex-md-row mb-4 box-shadow h-md-250 row justify-content-center">
            <div class="card-body d-flex flex-column align-items-start col-6">
                <h1 class="mb-0 d-inline-block mb-2 text-primary" style="font-weight:bold;">{{ $show->name }}</h1>
                <br/>
                <h4 class="text-bold">Synopsis :</h4>
                <p class="card-text mb-auto">
                    {{ $show->overview }}
                </p>
                <div class="mb-1 text-muted">{{ $show->cast }}</div>
                <a href="{{ route('home') }}" type="button" class="btn btn-sm btn-outline-secondary">Retour</a>
            </div>
            <div style="width: 50%;margin: auto;"">
                <img class="bd-placeholder-img card-img-top" src="{{'https://image.tmdb.org/t/p/w500/'.$show['poster_path']}}"> </img>
            </div>
        </div>
    </div>
</div>

<div class="movie-info">
    
        <h5>Commentaires</h5>
        @forelse($show->comments as $comment)
            <div class="card mb-2 ">
                <div class="card-body">
                    {{$comment->content}}
                    <div class="d-flex justify-content-between align-item-center">
                        <small>posté le {{$comment->created_at->format('d/m/Y')}}</small>
                        <span class="badge badge-primary">{{ $comment->user->name}}</span>
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-info">Aucun commentaire pour cette serie</div>
        @endforelse
        
        <form action="{{ route('comments.store', $show) }}" method="POST" class="mt-3">
        @csrf 
        <div class="form-group">
            <label for ="content"> Votre commentaire </label>
            <input type="hidden" name="commentable_id" value="{{ $show->id }}"/> 
            <textarea class="form-control" @error('content') is-invalid @enderror" name="content" id ="content" rows="5"></textarea>
        @error('content')
            <div class="invalid-feedback"> {{ $errors->first('content') }}</div>
        @enderror
        </div>
        
        <button type="submit" class="btn btn-primary"> Soumettre mon commentaire </button>
        </form>
    </div>

@endsection