<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home'); 

Route::get('/movies', 'MoviesController@index')->name('movies');
Route::get('/series', 'SeriesController@index')->name('series');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/show', 'HomeController@movies')->name('movies');

Route::get('/favoris', 'FormController@create')->name('favoris');
Route::post('/add_favoris', 'FormController@add_favoris')->name('add_favoris');

Route::get('/showTv/{id}', 'ShowController@showTv')->name('showTv');
Route::get('/showMovie/{id}', 'MovieController@showMovie')->name('showMovie');

Route::get('/favoris_genre_form', 'FormController@form_update_genre')->name('favoris_genre_form');
Route::post('/update_favoris', 'FormController@update_favoris')->name('update_favoris');

Route::get('/recommended_movies', 'MovieController@recommended_movies')->name('recommended_movies');
Route::get('/addMovieFav/{id}', 'FavoriController@addMovieFav')->name('addMovieFav');
Route::get('/favorites', 'FavoriController@showFav')->name('showFav');

Route::post('/comments/{movies}', 'CommentController@store')->name('comments.store');
Route::post('/comments/{shows}', 'CommentController@storeShow')->name('comments.storeShow');
