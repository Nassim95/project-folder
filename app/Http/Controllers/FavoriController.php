<?php

namespace App\Http\Controllers;

use App\Favori;
use App\User;
use App\Movie;
use App\Show;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class FavoriController extends Controller
{
    public function addMovieFav($id){
        $favoriMovie = new Favori();
        
        //get the movie which belongs to the $id
        $movie = (new Movie())->show($id);

        $favoriMovie->id_user = Auth::id();
        $favoriMovie->id_movie = $movie->id;

        $favoriMovie->save();
        
        return redirect('home');
    }


    public function showFav(){
        $userFav = User::find(Auth::id())->favoris;

        // $showFavoris = Favori::whereNotNull('id_show')->whereHas('user', function (Builder $query) {
        //     $query->where('id', '=', Auth::id());
        // })->get();

        $movie = new Movie();
        $favResult = [];

        foreach($userFav as $fav){
            array_push($favResult, $movie->show($fav->id_movie));
        }

        // $show = new Show();
        // $favShowResult = [];

        // foreach($showFavoris as $fav){
        //     array_push($favShowResult, $show->show($fav->id_show));
        // }

        return view('movie-favori', [
            'favResult' => $favResult
            // 'favShowResult' => $favShowResult
            ]);
    }
}
