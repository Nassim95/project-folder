<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $name = 'Nassim';
        return \view('profile', ['name' => $name]);
    }
}
