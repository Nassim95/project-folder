<?php

namespace App\Http\Controllers;

use App\Favori;
use Faker\ORM\CakePHP\Populator;
use Illuminate\Http\Request;
use App\Movie;
use App\Genre;
use App\Show;
use App\User;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\ShowController;
use Illuminate\Support\Facades\Http;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $popularShows = Show::getPopular();
        $popularMovies = Movie::getPopular();
        $nowPlayingMovies = Movie::getNowPlaying();

        $recommendedMovies = MovieController::recommended_movies();
        $recommendedShows = ShowController::recommended_shows();

        return view('home',['popularMovies'=>$popularMovies, 'popularShows' => $popularShows, 'recommendedMovies' => $recommendedMovies, 'recommendedShows' => $recommendedShows]);
    }

}
