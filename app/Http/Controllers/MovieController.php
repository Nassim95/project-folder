<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Favori;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Builder;

class MovieController extends Controller
{
    public function showMovie($id){
        $movie = (new Movie())->show($id);
        $recommendedMovies = self::recommended_movies();
        return view('show-movie', ['movie' => $movie, 'recommendedMovies' => $recommendedMovies]);
    }

    public static function recommended_movies()
    {
        $movies = Movie::getPopular();

        $myMovie = $movies->all();
        
        shuffle($myMovie);

        $recommendedMovies = array_slice($myMovie, 0, 4);


        $favoris = Favori::whereNotNull('id_genre')->whereHas('user', function (Builder $query) {
            $query->where('id', '=', Auth::id());
        })->get();


        $favorisGenreId = collect($favoris)->map(function ($favori) {
            return $favori->id_genre;
        });

    
        $recommended = collect($recommendedMovies)->filter(function($movie) use ($favorisGenreId){
            $in = false;
            foreach ($movie->genres as $genre) {
                if (in_array($genre->id, $favorisGenreId->all())) {
                    $in = true;
                    break;
                }
            }
            if ($in) {
                return $movie;
            }
        });

        return $recommended;
    }
}
