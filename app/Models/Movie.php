<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Movie
 * 
 * @property int $id
 * @property float $popularity
 * @property int $vote_count
 * @property bool $video
 * @property string $poster_path
 * @property bool $adult
 * @property string $backdrop_path
 * @property string $original_language
 * @property string $original_title
 * @property string $title
 * @property float $vote_average
 * @property string $overview
 * @property Carbon $release_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Movie extends Model
{
	protected $table = 'movies';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'popularity' => 'float',
		'vote_count' => 'int',
		'video' => 'bool',
		'adult' => 'bool',
		'vote_average' => 'float'
	];

	protected $dates = [
		'release_date'
	];

	protected $fillable = [
		'popularity',
		'vote_count',
		'video',
		'poster_path',
		'adult',
		'backdrop_path',
		'original_language',
		'original_title',
		'title',
		'vote_average',
		'overview',
		'release_date'
	];
}
