<?php

/**
 * Created by Reliese Model.
 */

namespace App;

use Barryvdh\Debugbar\Twig\Extension\Dump;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class Movie
 * 
 * @property int $id
 * @property float $popularity
 * @property int $vote_count
 * @property bool $video
 * @property string $poster_path
 * @property bool $adult
 * @property string $backdrop_path
 * @property string $original_language
 * @property string $original_title
 * @property string $title
 * @property float $vote_average
 * @property string $overview
 * @property Carbon $release_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Movie extends Model
{
	protected $table = 'movies';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'popularity' => 'float',
		'vote_count' => 'int',
		'video' => 'bool',
		'adult' => 'bool',
		'vote_average' => 'float'
	];

	protected $dates = [
		'release_date'
	];

	protected $fillable = [
		'id',
		'popularity',
		'vote_count',
		'video',
		'poster_path',
		'adult',
		'backdrop_path',
		'original_language',
		'original_title',
		'title',
		'vote_average',
		'overview',
		'release_date'
	];

	public $genres;
	public $credits;
	public $videos;
	public $images;

	public static function getPopular()
    {
        $popularMoviesArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/popular')
		->json()['results'];

		$popularMovies = collect($popularMoviesArray)->map(function($data){
			$movie = new Movie();
			$genresMovie = Arr::pull($data,'genre_ids');
			$genres = Genre::getMoviesGenres();
			$movie->genres = collect($genres)->filter(function($genre) use ($genresMovie) {
				if(in_array($genre->id, $genresMovie))
				{
					return ($genre);
				}
			
			});		
			return $movie->fill(Arr::except($data,['genre_ids']));
		});
		return $popularMovies;
    }

    public static function getNowPlaying()
    {
        $nowPlayingArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/now_playing')
		->json()['results'];
		
		$nowPlaying = collect($nowPlayingArray)->map(function($data) {
			$movie = new Movie();
			$genresMovie = Arr::pull($data,'genre_ids');
			$genres = Genre::getMoviesGenres();

			$movie->genres = collect($genres)->filter(function($genre) use ($genresMovie) {
				if(in_array($genre->id, $genresMovie))
				return ($genre);
			});
			return $movie->fill($data);
		});
		return $nowPlaying;
	}

	public static function show($id)
    {
		$movieArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/'.$id.'?append_to_response=credits,videos,images')
		->json();
			$movie = new Movie(); 
			$movie->credits = Arr::pull($movieArray,'credits');
			$movie->videos = Arr::pull($movieArray,'videos');
			$movie->images = Arr::pull($movieArray,'images');
			$genresMovie = Arr::pull($movieArray,'genres');
			$genres = Genre::getMoviesGenres();
			$movie->genres = collect($genres)->filter(function($genre) use ($genresMovie) {
				if(in_array($genre->id, $genresMovie))
				{
					return ($genre);
				}
			});		
			return $movie->fill(Arr::except($movieArray,['genre_ids']));
	}

	public function details()
	{

	}

	public function image($width = 500)
    {  
        return "https://image.tmdb.org/t/p/w$width/$this->poster_path";
    }
	
	public function genre()
	{
		return $this->belongsTo('App\Genre','id_genre');
	}
	
	/* recuperer les commentaires les plus recents*/
	public function comments()
	{
		// return $this->morphMany('App\Comment', 'commentable')->latest();
		return $this->hasMany('App\Comment', 'commentable_id');
		
	}

}
